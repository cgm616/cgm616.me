---
title: Running rust on the BBC microbit
date: 2018-07-12
draft: true
---

Support crate for microbit: https://github.com/therealprof/microbit
Tutorial for rust on embedded: http://blog.japaric.io/quickstart/
Installing `arm-none-eabi-gdb` with Python support: https://gist.github.com/JayKickliter/8004bafaf3d365dc8fe23843fae15c67
GDB dashboard: https://github.com/cyrus-and/gdb-dashboard
#[deny(warnings)] on docs
display with interrupts
allocation and static and trait objects
transition to a library
examples as tests
RTC module