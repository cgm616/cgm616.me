+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/progressive-values-how-to-win-in-2020/"
redirect_to = "/2019/02/how-to-win-in-2020/"
redirect_enabled = true
private = true
+++