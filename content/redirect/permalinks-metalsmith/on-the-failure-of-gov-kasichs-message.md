+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/on-the-failure-of-gov-kasichs-message/"
redirect_to = "/2019/02/on-the-failure-of-gov-kasichs-message/"
redirect_enabled = true
private = true
+++