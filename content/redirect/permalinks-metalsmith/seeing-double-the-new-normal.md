+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/seeing-double-the-new-normal/"
redirect_to = "/2020/04/seeing-double-the-new-normal/"
redirect_enabled = true
private = true
+++