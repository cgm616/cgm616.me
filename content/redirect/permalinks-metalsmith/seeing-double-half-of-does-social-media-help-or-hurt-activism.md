+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/seeing-double-half-of-does-social-media-help-or-hurt-activism/"
redirect_to = "/2020/05/seeing-double-half-of-does-social-media-help-or-hurt-activism/"
redirect_enabled = true
private = true
+++