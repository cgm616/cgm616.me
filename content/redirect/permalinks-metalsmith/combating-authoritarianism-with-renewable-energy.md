+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/combating-authoritarianism-with-renewable-energy/"
redirect_to = "/2019/03/combating-authoritarianism-with-renewable-energy/"
redirect_enabled = true
private = true
+++