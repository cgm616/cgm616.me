+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/working-with-service-workers/"
redirect_to = "/2017/07/working-with-service-workers/"
redirect_enabled = true
private = true
+++