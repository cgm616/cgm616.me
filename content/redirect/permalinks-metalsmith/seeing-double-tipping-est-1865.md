+++
# For example, to Redirect from /old_blog to /blog, set 
# url to "/old_blog" and redirect_to to "/blog" below
type = "redirect"
url = "/articles/seeing-double-tipping-est-1865/"
redirect_to = "/2020/03/seeing-double-tipping-est-1865/"
redirect_enabled = true
private = true
+++