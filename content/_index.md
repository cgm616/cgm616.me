---
title: Home
---

My name is Cole Graber-Mitchell (cgm616).

I'm a postgrad student in the Anthropology department at the London School of
Economics and Politics Science.
I graduated recently from Amherst College in Amherst, Massachusetts, and I'm
originally from Minneapolis, Minnesota.

I'm interested in progressive organizing and academics; I also like to dabble in
programming, block printing, singing, and guitar.

Right now, I'm thinking most about what I've come to term "climate humanities":
theoretical and practical work—in the arts, academia, politics, and
elsewhere—towards building a society that can weather the climate crisis.

To see my academic work, go to [my research page](/research).

{{< a2 "Other places you can find me" >}}

- [Twitter](https://twitter.com/cgm616), if you want to read my short thoughts
- [GitHub](https://github.com/cgm616), to see most of my programming projects
- [GitLab](https://gitlab.com/cgm616), to see some more of my programming projects
- [Keybase](https://keybase.io/cgm616/), with links to my PGP key and other proofs about my identity
- [LinkedIn](https://www.linkedin.com/in/cgm616), for my resume and work experience
