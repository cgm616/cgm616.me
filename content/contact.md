---
title: Contact
---

To contact me, either use one of the services on the [front page](/) or one of the methods listed below.

- Phone number for calls or SMS: `+1 (413) 248-6639`
- Email (feel free to encrypt with my [public key](/pgp_key.asc)): `website@cgm616.me`
- Telegram with username [cgm616](https://t.me/cgm616)

To contact me over an encrypted service, please either use PGP over email, Telegram or [Keybase](https://keybase.io/cgm616/).

